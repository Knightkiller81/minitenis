import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;


public class Raqueta {
	private int x;
	private int xa;
	private static final int Y=330;
	private static final int ANCHO=60;
	private static final int ALTO=10;
	private Juego game;
	public Raqueta(Juego game){
		this.game=game;
		x=40;
		xa=0;
	}
	
	public void move(){
		if (x + xa > 0 && x + xa < game.getWidth()-ANCHO)
		x+=xa;
	}
	
	public void paint(Graphics2D g){
		g.fillRect(x,Y,ANCHO,ALTO);
	}
	public void keyPressed(KeyEvent e){
		if(e.getKeyCode()==KeyEvent.VK_RIGHT)
			xa=game.getVelocidad();
		if(e.getKeyCode()==KeyEvent.VK_LEFT)
			xa=-game.getVelocidad();
	}
	public void keyReleased(KeyEvent e){
		xa=0;
	}
	public Rectangle hitbox(){
		return new Rectangle(x,Y,ANCHO,ALTO);
	}
	public int getTopY(){
		return Y;
	}
}
