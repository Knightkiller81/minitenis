import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

//TODO Maximas Puntuaciones;
public class Ventana extends JFrame {
	private JLabel lblPuntuacion;
	private Juego game;
	private Timer tJugar;
	
	public Ventana(){
		super("Minitenis V1");
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		game=new Juego();
		this.add(game,BorderLayout.CENTER);
		
		lblPuntuacion=new JLabel(String.format("Puntuacion:%d",game.getPuntuacion()));
		this.add(lblPuntuacion,BorderLayout.SOUTH);
		
		tJugar = new Timer (10, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				lblPuntuacion.setText(String.format("Puntuacion:%d",game.getPuntuacion()));
				game.repaint();
				game.mover();
			}
		});
		this.setSize(game.getWidth(),game.getHeight()+20);
		tJugar.start();
	}
}
