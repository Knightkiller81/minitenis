import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
@SuppressWarnings("serial")
/**
 * Panel del juego
 * @author tor
 * @version 0.1a 24/04/2014
 */
public class Juego extends JPanel {
	 Bola bola;
	Raqueta raqueta;
	private int puntuacion;
	private int velocidad;
	
	public Juego(){
		this.setSize(300,400);
		puntuacion=0;
		this.setVelocidad(1);
		
		raqueta=new Raqueta(this);
		
		bola=new Bola(this);
		this.addKeyListener(new KeyListener(){

			@Override
			public void keyPressed(KeyEvent e) {
				raqueta.keyPressed(e);
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				raqueta.keyReleased(e);
				
			}

			@Override
			public void keyTyped(KeyEvent e) {			
			}
		});
		this.setFocusable(true);
		Sonidos.FONDO.loop();
	}
	
	public int getPuntuacion() {
		return puntuacion;
	}
	public void setPuntuacion(int puntuacion) {
		this.puntuacion = puntuacion;
	}
	
	public int getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}
	/**
	 * Mueve la raqueta y la bola a las posiciones correspondientes.
	 */
	public void mover(){
		bola.move();
		raqueta.move();
	}
	/**
	 * Aumenta la velocidad del juego
	 */
	public void aumentarVelocidad(){
		this.setVelocidad(this.getVelocidad()+1);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		bola.paint(g2d);
		raqueta.paint(g2d);
	}
	/**
	 * Muestra ventana de Fin de Juego y reproduce sonido
	 */
	public void GameOver(){
		Sonidos.FONDO.stop();
		Sonidos.GAMEOVER.play();
		JOptionPane.showMessageDialog(this, 
									"Has perdido,lo sentimos pero por ahora no registramos puntuaciones",
									"Has perdido",
									JOptionPane.ERROR_MESSAGE, 
									null);
		this.setPuntuacion(0);
		this.setVelocidad(1);
		bola=new Bola(this,10,10);
		Sonidos.FONDO.loop();
	}

	
}
