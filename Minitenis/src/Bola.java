import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;


public class Bola {
	private int x;
	private int y;
	private int xa;
	private int ya;
	private static final int DIAMETRO=15;
	private boolean rebote;
	private Juego game;
	
	public Bola(Juego game2){
		this.game=game2;
		 x = 0;
		 y = 0;
		 xa = 1;
		 ya = 1;
	}
	public Bola(Juego game2,int x,int y){
		this.game=game2;
		this.x = x;
		this.y = y;
		 xa = 1;
		 ya = 1;	
	}
	public void move() {
		rebote=true;
		if (x + xa < 0)
			xa = game.getVelocidad();
		else{
			if (x + xa > game.getWidth() - 15)
				xa =-game.getVelocidad();
			else{
				if (y + ya < 0)
					ya = game.getVelocidad();
				else{
					if (y + ya > game.getHeight() -DIAMETRO)
						game.GameOver();
					else{
						if(colision()){
							game.setPuntuacion(game.getPuntuacion()+1);
							y=game.raqueta.getTopY()-DIAMETRO;
							if(game.getPuntuacion()%5==0){
								Sonidos.LEVELUP.play();
								game.aumentarVelocidad();
							}
							ya=-game.getVelocidad();
						}
						else
							rebote=false;
					}
				}
			}
		}
		if(rebote)
			Sonidos.REBOTE.play();
		x = x + xa;
		y = y + ya;
	}
	public int getX() {
		return x;
	}
	private boolean colision(){
		return game.raqueta.hitbox().intersects(this.hitbox());
	}
	
	private Rectangle hitbox(){
		return new Rectangle(x,y,DIAMETRO,DIAMETRO);
	}
	public void paint(Graphics2D g) {
		g.setColor(Color.BLUE);
		g.fillOval(x, y,DIAMETRO,DIAMETRO);
	}
}
