import java.applet.Applet;
import java.applet.AudioClip;


public class Sonidos {
	public static final AudioClip FONDO=Applet.newAudioClip(Sonidos.class.getResource("fondo.wav"));
	public static final AudioClip REBOTE=Applet.newAudioClip(Sonidos.class.getResource("rebote.wav"));
	public static final AudioClip GAMEOVER=Applet.newAudioClip(Sonidos.class.getResource("gameover.wav"));
	public static final AudioClip LEVELUP=Applet.newAudioClip(Sonidos.class.getResource("lvlup.wav"));
}
